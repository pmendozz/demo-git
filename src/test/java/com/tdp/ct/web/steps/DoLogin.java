package com.tdp.ct.web.steps;

import com.tdp.ct.web.page.Pages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DoLogin {

    @Autowired
    private Pages pages;

    public void withCredentials(String user, String pass){
        pages.loginPage().typeUser(user);
        pages.loginPage().typePass(pass);
        pages.loginPage().login();
    }

}
