package com.tdp.ct.web.page;

import com.tdp.ct.web.base.WebBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends WebBase {

    @FindBy(id = "user-name")
    private WebElement userName;
    @FindBy(id = "password")
    private WebElement password;
    @FindBy(id = "login-button")
    private WebElement botonlogin;

    public void typeUser(String user){
        type(userName, user, 15);
    }

    public void typePass(String pass){
        type(password, pass, 15);
    }

    public void login(){
        click(botonlogin);
    }

}