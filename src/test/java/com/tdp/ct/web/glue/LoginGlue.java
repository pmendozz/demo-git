package com.tdp.ct.web.glue;

import com.tdp.ct.web.lib.WebDriverManager;
import com.tdp.ct.web.steps.DoLogin;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@CucumberContextConfiguration
public class LoginGlue {

    @Autowired
    private WebDriverManager manager;

    @Autowired
    private DoLogin doLogin;

    @Given("que el usuario se encuentra en la pagina Saucedemo")
    public void queElUsuarioSeEncuentraEnLaPaginaSaucedemo() {
        manager.setUpDriver();
        manager.navigateTo("https://www.saucedemo.com/");
    }

    @When("ingrese sus credenciales {string}, {string} e inicie sesión")
    public void ingreseSusCredencialesEInicieSesión(String user, String pass) {
        doLogin.withCredentials(user, pass);
    }

    @Then("debería ver el titulo de {string}")
    public void deberíaVerElTituloDe(String arg0) {

    }
}